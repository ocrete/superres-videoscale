/* GStreamer
 * Copyright (C) <1999> Erik Walthinsen <omega@cse.ogi.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __COL_SUPER_RES_VIDEO_SCALE_H__
#define __COL_SUPER_RES_VIDEO_SCALE_H__

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/video/gstvideofilter.h>

G_BEGIN_DECLS


#define COL_TYPE_SUPER_RES_VIDEO_SCALE \
  (col_super_res_video_scale_get_type())
G_DECLARE_FINAL_TYPE(ColSuperResVideoScale, col_super_res_video_scale, COL,
    SUPER_RES_VIDEO_SCALE, GstVideoFilter);


/**
 * ColSuperResVideoScale:
 *
 * Private data structure
 */
struct _ColSuperResVideoScale {
  GstVideoFilter element;

  /* MARCUS: Replace this with your data structures/pointers */
  GstVideoConverter *convert;
};


G_GNUC_INTERNAL GType col_super_res_video_scale_get_type (void);

G_END_DECLS

#endif /* __COL_SUPER_RES_VIDEO_SCALE_H__ */
